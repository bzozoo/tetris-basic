(function () {
const application = { name: "TETRIS BASIC", version: "v47" };

const gameSettings = {
   rerenderMode: "onelinebased", // usage: [onelinebased, squarebased, old]
   rerenderable: true,
   width: 10,
   gridNumbers: 250,
   lineNumbers: () => { return gameSettings.gridNumbers / gameSettings.width },
   controllButtonsDeactivable: true,
   tetrominoDropable: true,
   displayWidth: 6,
   displayIndex: 1,
   boomm: 'https://raw.githubusercontent.com/bzozoo/Tetris-Basic/master/images/boomm.gif',
   rewardExtraPics: [
      "https://i.imgur.com/awaQZST.jpg",
      "https://i.imgur.com/psZNoKt.jpg",
      "https://i.imgur.com/uCvHYUX.jpg",
      "https://i.imgur.com/rW0gv4H.jpg",
      "https://i.imgur.com/TpZDy0N.jpg",
      "https://i.imgur.com/2EjNoQG.jpg",
      "https://i.imgur.com/KBY1sRA.jpg",
      "https://i.imgur.com/h8SWCow.jpg",
      "https://i.imgur.com/UkUQbSO.jpg",
      "https://i.imgur.com/YEcYcxl.jpg",
      "https://i.imgur.com/voXqUm3.jpg",
      "https://i.imgur.com/cTMV0N2.jpg",
      "https://i.imgur.com/QQfe4tR.jpg",
      "https://i.imgur.com/eaIsfFZ.jpg",
      "https://i.imgur.com/whs4Oze.jpg",
      "https://i.imgur.com/5EvLQKA.jpg",
      "https://i.imgur.com/XMr4syr.jpg",
      "https://i.imgur.com/CuC6EMI.jpg",
      "https://i.imgur.com/uw8zEr8.jpg",
      "https://i.imgur.com/eUgVtPe.jpg"
   ],
};

main(gameSettings); function main(gameSettings){

const {rerenderMode, controllButtonsDeactivable, gridNumbers, width, displayWidth, displayIndex, lineNumbers, rewardExtraPics, boomm } = gameSettings;
console.log("RENDER MODE ::: " + rerenderMode);

//Declare DOM elements
const grid = document.querySelector(".grid");
const miniSquares = Array.from(document.querySelectorAll(".mini-grid div"));
const miniGrid = document.querySelector(".mini-grid");
const startBtn = document.querySelector("#start-button");
const invertCheckBox = document.querySelector("#invert");
const rewardCheckBox = document.querySelector("#reward");
const displaySquares = document.querySelectorAll(".mini-grid div");

const gameTimer = new IntervalEncapsulator(tryFreeze, 500);
const speeder = new IntervalEncapsulator(tryFreeze, 100);
const musicInterval = new IntervalEncapsulator(playMusic, 1);
const run = backcounter(3);

const currents = {
   random: null,
   nextRandom: 0,
   currentPosition: 4,
   currentRotation: 0,
   nextRotation: this.currentRotation + 1,
   current: null,
   nextRotatedCurrent: null
}

let {random,nextRandom,currentPosition,currentRotation,nextRotation,current,nextRotatedCurrent} = currents;
let score = 0;

//The Tetrominoes
/* lTetronimo is the example *
                 0 1 2    0 1 2    0 1 2    0 1 2
                |--------------------------------    
            0   |  X X               X          
        width   |  X      X X X      X      X
    2 * width   |  X          X    X X      X X X        
    */
const lTetromino = [
   [1, width + 1, width * 2 + 1, 2],
   [width, width + 1, width + 2, width * 2 + 2],
   [1, width + 1, width * 2 + 1, width * 2],
   [width, width * 2, width * 2 + 1, width * 2 + 2]
];

const zTetromino = [
   [0, width, width + 1, width * 2 + 1],
   [width + 1, width + 2, width * 2, width * 2 + 1],
   [0, width, width + 1, width * 2 + 1],
   [width + 1, width + 2, width * 2, width * 2 + 1]
];

const tTetromino = [
   [1, width, width + 1, width + 2],
   [1, width + 1, width + 2, width * 2 + 1],
   [width, width + 1, width + 2, width * 2 + 1],
   [1, width, width + 1, width * 2 + 1]
];

//The Tetrominoes
/* oTetronimo is the example *
                 0 1 2    0 1 2    0 1 2    0 1 2
                |--------------------------------    
            0   |X X      X X      X X      X X         
        width   |X X      X X      X X      X X
    2 * width   |       
    */

const oTetromino = [
   [0, 1, width, width + 1],
   [0, 1, width, width + 1],
   [0, 1, width, width + 1],
   [0, 1, width, width + 1]
];

const iTetromino = [
   [1, width + 1, width * 2 + 1, width * 3 + 1],
   [width - 1, width, width + 1, width + 2],
   [1, width + 1, width * 2 + 1, width * 3 + 1],
   [width - 1, width, width + 1, width + 2]
];

const liTetromino = [
   [0, 1, width + 1, width * 2 + 1],
   [2, width, width + 1, width + 2],
   [0, width, width * 2, width * 2 + 1],
   [0, 1, 2, width]
];

const ziTetromino = [
   [1, width, width + 1, width * 2],
   [0, 1, width + 1, width + 2],
   [1, width, width + 1, width * 2],
   [0, 1, width + 1, width + 2]
];

//Tetromino pair with colors arrays
const theTetrominoes = [
   lTetromino,
   zTetromino,
   tTetromino,
   oTetromino,
   iTetromino,
   liTetromino,
   ziTetromino
];
const tetrominoNames = ["L", "Z", "T", "O", "I", "L-INVERT", "Z-INVERT"];
const colors = [
   "orange",
   "red",
   "purple",
   "green",
   "blue",
   "brown",
   "turquoise"
];


//the Tetrominos without rotations
const upNextTetrominoes = [
   [
      2 * displayWidth + 1,
      2 * displayWidth + 2,
      3 * displayWidth + 1,
      4 * displayWidth + 1
   ], //lTetromino
   [
      2 * displayWidth + 1,
      3 * displayWidth + 1,
      3 * displayWidth + 2,
      4 * displayWidth + 2
   ], //zTetromino
   [
      2 * displayWidth + 1,
      3 * displayWidth,
      3 * displayWidth + 1,
      3 * displayWidth + 2
   ], //tTetromino
   [
      2 * displayWidth + 1,
      2 * displayWidth + 2,
      3 * displayWidth + 1,
      3 * displayWidth + 2
   ], //oTetromino
   [
      1 * displayWidth + 1,
      2 * displayWidth + 1,
      3 * displayWidth + 1,
      4 * displayWidth + 1
   ], //iTetromino
   [
      2 * displayWidth + 1,
      2 * displayWidth + 2,
      3 * displayWidth + 2,
      4 * displayWidth + 2
   ], // liTetromino
   [
      2 * displayWidth + 2,
      3 * displayWidth + 1,
      3 * displayWidth + 2,
      4 * displayWidth + 1
   ] // ziTetromino
];

//InitGame
initGame();
////

///InitDevFunctions
//numberisedSquares();
//numberisedOneline();
//makeTestLines()
//arrToArg([230, 238])
//checkCurrent()
////

function initGame() {
   console.log("Init Game");
   version.innerText = application.version;
   grid.innerHTML = TemplateForGrid();
   miniSquaresInit();
   gameOverDiv.style.display = "none";
   scoreInit();
   gameTimer.stop();
   currentPosition = 4;
   currentRotation = 0;
   random = Math.floor(Math.random() * theTetrominoes.length);
   nextRandom = Math.floor(Math.random() * theTetrominoes.length);
   current = theTetrominoes[random][currentRotation];
   nextRotatedCurrent = theTetrominoes[random][nextRotation];
   startBtn.innerHTML = "▶ START";
   deactivateGameButtons();
   rewardPicDivSetHeight(0);
   rewardPicDivSetBackground();
   rewardCheckBox.checked = checkRewardExtra();
   weird.checked = checkWeirdMode();
}

function checkWeirdMode(){
   return localStorage.getItem("storedWeirdMode") === null? false : JSON.parse(localStorage.getItem("storedWeirdMode"));
}

function checkRewardExtra(){
   return localStorage.getItem("storedReward") === null ? true : JSON.parse(localStorage.getItem("storedReward"));
}

function rewardPicDivSetBackground() {
   randomReward = Math.floor(Math.random() * rewardExtraPics.length);
   rewardPicDiv.style.backgroundImage =
      "url(" + rewardExtraPics[randomReward] + ")";
}

function scoreInit() {
   score = 0;
   scoreDisplay.innerHTML = score;
}

function miniSquaresInit() {
   miniSquares.forEach(function (miniSquare, sqkey) {
      miniSquares[sqkey].style = "";
      miniSquares[sqkey].classList.remove("tetromino");
   });
}

function draw() {
   const {squares} = getDOMElements();
   current.forEach((index) => {
      squares[currentPosition + index].classList.add("tetromino");
      squares[currentPosition + index].style.backgroundColor = colors[random];
   });
}

function undraw() {
   const {squares} = getDOMElements();
   current.forEach((index) => {
      squares[currentPosition + index].classList.remove("tetromino");
      squares[currentPosition + index].style.backgroundColor = "";
   });
}

function moveLeft() {
   undraw();
   const {squares} = getDOMElements();
   const isAtLeftEdge = current.some(
      (index) => (currentPosition + index) % width === 0
   );
   if (!isAtLeftEdge) currentPosition -= 1;
   if (current.some((index) => squares[currentPosition + index].classList.contains("taken"))){
      currentPosition += 1;
   }
   draw();
}

function moveRight() {
   undraw();
   const {squares} = getDOMElements();
   const isAtRightEdge = current.some(
      (index) => (currentPosition + index) % width === width - 1
   );
   if (!isAtRightEdge) currentPosition += 1;
   if (current.some((index) => squares[currentPosition + index].classList.contains("taken"))){
      currentPosition -= 1;
   }
   draw();
}

function moveDown() {
   undraw();
   currentPosition += width;
   draw();
}

function tryFreeze() {
   (lockCheck() === true) && doThisIfLockable();
   (lockCheck() === false) && moveDown();   
}

function lockCheck() {
   return checkCurrentInSomeInNextRow() ? true : false;
}

function checkCurrentInSomeInNextRow(){
   const { squares } = getDOMElements();
   return current.some((index) => squares[currentPosition + index + width].classList.contains("taken"))
}

function doThisIfLockable(){
   //console.log("doThisIfLockable")
   addCurrentToTaken(); //Lock current
   tryGameOver();
   let score = 0;
   
   let rowsInTaken = checkRowsInTaken();
   (rowsInTaken.length) && doThisIfRowInTaken(rowsInTaken)
   
   setRewardPic();
   
   (!checkGameOver()) && dropNewTetromino();
}
   
function addCurrentToTaken() {
   const {squares} = getDOMElements();
   current.forEach((index) =>
      squares[currentPosition + index].classList.add("taken")
   );
}

function doThisIfRowInTaken(rowsInTaken){
      fasterDownStop();
      tryEffect(rowsInTaken);
      breakBeforeRender();
      let rendered = doRender(rowsInTaken);
      rendered && addScore(rowsInTaken.length);
}

function tryEffect(rowsInTaken){
   (run.getActualCount() === (run.getDefaultCount)) && doEffect(rowsInTaken)
}

function doEffect(rowsInTaken){
      console.log('Do EFFECT ::: ');
      let colorizeThis = (rerenderMode === "squarebased")? rowsInTaken.map((rowInTaken) => [squareToOnelineNum(rowInTaken[1])]) : rowsInTaken; 
      colorizeThis.map((rowInTaken) =>
            getOneline(rowInTaken).map((cell)=> 
                   cell.style.backgroundImage = `url("${boomm}")`
            )
       )
      return true;
}

////NEW BREAKCOUNTER
function breakBeforeRender(){
   console.log("ACTUAL BREAKCOUNT ::: " + run.getActualCount())
   if(run.getActualCount() > 0){
      gameSettings.rerenderable = false;
      gameSettings.tetrominoDropable = false;
      run.doBackCount()
   }
   if(run.getActualCount() === 0){
      console.log("REACTIVATE... Renderable and Tetromino dropable...")
      gameSettings.rerenderable = true;
      gameSettings.tetrominoDropable = true;
      run.restartBackCount();
   }
   //console.log('BEAKCOUNT ::: ' + run.getActualCount())
}
///

function doRender(rowsInTaken){
   //console.log("DO RENDER")
   if(gameSettings.rerenderable){
      switch(rerenderMode) {
         case "onelinebased":
            //OnelineBasedRerender
            rerenderOnelines(rowsInTaken);
            return true;
         break;
         case "squarebased":
            //SquareBasedRerender
            rerenderSquares(rowsInTaken);
            return true;
         break;
         case "old":
            //OldRerender
            rerenderGrids();
            return true;
         break;
         default:
            return false;
      }
   }
   
   return false;
}

function checkRowsInTaken(){
   switch(rerenderMode) {
         case "onelinebased":
            //OnelineBased checker
            return whichOnelinesInTaken();
         break;
         case "squarebased":
            //SquareBased checker
            return whichSquareInTaken();
         break;
         case "old":
            //Old no checker!
            return false;
         break;
         default:
            return false;
      }
}

function addScore(lines) {
   score += lines*10;
   scoreDisplay.innerHTML = score;
}

function setRewardPic() {
   (checkRewardExtra()) && rewardPicDivSetHeight(score / 3);
}

function rewardPicDivSetHeight(value) {
   rewardPicDiv.style.height = value + "%";
}

function dropNewTetromino() {
   if (gameSettings.tetrominoDropable === true) {
      random = nextRandom;
      nextRandom = Math.floor(Math.random() * theTetrominoes.length);
      current = theTetrominoes[random][currentRotation];
      currentPosition = 4;
      draw();
      displayShape();
   }
}

//OnelineBasedRerender
function rerenderOnelines(whichOnelinesInTaken){
   whichOnelinesInTaken.map((oneline) => { 
      removeOnelineFromTaken(oneline);
      (!checkWeirdMode()) && jumpOnelineToFirst(oneline);
      removeOnelineFromTetromino(oneline);
   })
   return whichOnelinesInTaken.length;
}

function removeOnelineFromTaken(oneline){ [...grid.querySelectorAll(`[data-oneline="${oneline}"]`)].map((cell) => cell.classList.remove('taken')); }

function removeOnelineFromTetromino(oneline){ [...grid.querySelectorAll(`[data-oneline="${oneline}"]`)].map((cell) => { cell.classList.remove('tetromino'); cell.style=""; }); }

function jumpOnelineToFirst(oneline){
   let firstSquare = getDOMElements().firstSquare()
   getOneline(oneline).map((cell)=> firstSquare.insertAdjacentElement('beforebegin', cell));
   //syncSuqares();
}

function whichOnelinesInTaken(){
   //Check only in Activegameplace!
   let takenLineList = [];
   for (let oneline = 4; oneline < lineNumbers(); oneline++) {
      let result = checkOnlelineInTaken(oneline);
      result && takenLineList.push(oneline);
   }
   return takenLineList;
}

function checkOnlelineInTaken(oneline){
   return [...grid.querySelectorAll(`[data-oneline="${oneline}"]`)].every((cell) => cell.classList.contains("taken"));
}

function getOneline(oneline){
   return [...grid.querySelectorAll(`[data-oneline="${oneline}"]`)].map((cell) => cell)
}

function getActivegameplaceOneline(oneline){
   return [...grid.querySelectorAll(`.activegameplace[data-oneline="${oneline}"]`)].map((cell) => cell)
}

//SquareBasedRerender
function rerenderSquares(rows){
   rows.map((row)=>{
         uncolorizeRow(row);
         (!checkWeirdMode()) && jumpSquaresToFirst(row);
   });
   return rows.length;
}

function whichSquareInTaken(){
   const {squares} = getDOMElements();
   let takenLineList = [];
   for (let i = 0; i < (squares.length - width - 1); i += width) {
      const row = [i, i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7, i + 8, i + 9];
      if (row.every((index) => squares[index].classList.contains("taken"))) {
         takenLineList.push(row);
      }
   }
   return takenLineList;
}

function uncolorizeRow(row){
     const {squares} = getDOMElements();
     row.forEach((index) => {
         squares[index].classList.remove("taken");
         squares[index].classList.remove("tetromino");
         squares[index].style.backgroundColor = "";
     });
}


function jumpSquaresToFirst(row){
   const { squares } = getDOMElements();
   const firstSquare = getDOMElements().firstSquare()
   row.forEach((index) => {
      firstSquare.insertAdjacentElement('beforebegin', squares[index]);
   });
}

//OldRerender
function rerenderGrids() {
   const {squares} = getDOMElements();
   let count = 0;
   for (let i = 0; i < 229; i += width) {
   const row = [i, i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7, i + 8, i + 9];

      if (row.every((index) => squares[index].classList.contains("taken"))) {
         console.log("ROW :" + row);

         count += 1;

         //Remove Classes and color from removed SQUARES
         row.forEach((index) => {
            squares[index].classList.remove("taken");
            squares[index].classList.remove("tetromino");
            squares[index].style.backgroundColor = "";
         });
         
         if(!checkWeirdMode()){
            //Remove Full Squares
            const squaresRemoved = squares.splice(i, width);

            //Remove hidden SQUARES from Line0-Line1
            const hiddenSquaresRemoved = squares.splice(0, width * 3);

            //Re-add to SQUARES the removed SQUARES
            squares = squaresRemoved.concat(squares);
            squares = hiddenSquaresRemoved.concat(squares);

            //Render new Squares
            squares.forEach((cell, key) => createNewGridLine(cell, key));
         }
     }
   }
   return count;
}

function createNewGridLine(cell, key) {
   //console.log("CELL IN createNewGridLine ::: ")
   //console.log(cell)
   grid.appendChild(cell);
}

///FIX ROTATION OF TETROMINOS A THE EDGE
function isAtRight() {
   return current.some((index) => (currentPosition + index + 1) % width === 0);
}

function isAtLeft() {
   return current.some((index) => (currentPosition + index) % width === 0);
}

//Rotate functions
function rotate() {
   undraw();
   if (nextRotationInCollision()) {
      console.log("I CAN NOT TO ROTATE");
   } else {
      currentRotation++;
      if (currentRotation === current.length) {
         //if the current rotation gets to 4, make it go back to 0
         currentRotation = 0;
      }
      current = theTetrominoes[random][currentRotation];
      rotationcorrection();
   }
   draw();
}

function rotationcorrection(P) {
   //console.log("CHECK ROTATED POSITION");
   //console.log(P);
   P = P || currentPosition; //get current position.  Then, check if the piece is near the left side.
   if ((P + 1) % width < 4) {
      //add 1 because the position index can be 1 less than where the piece is (with how they are indexed).
      if (isAtRight()) {
         //use actual position to check if it's flipped over to right side
         currentPosition += 1; //if so, add one to wrap it back around
         rotationcorrection(P); //check again.  Pass position from start, since long block might need to move more.
      }
   } else if (P % width > 5) {
      if (isAtLeft()) {
         currentPosition -= 1;
      }
   }
}

function nextRotationInCollision() {
   return (checkNextRotationInTaken())? true : false;
}

function checkNextRotationInTaken() {
   const {squares} = getDOMElements();
   nextRotation = (currentRotation === 3)? 0 : currentRotation + 1;
   nextRotatedCurrent = theTetrominoes[random][nextRotation];

   return (nextRotatedCurrent.some((index) => squares[currentPosition + index].classList.contains("taken")))? true : false;
}
/////////

//display the shape in the mini-grid display
function displayShape() {
   displaySquares.forEach((square) => {
      square.classList.remove("tetromino");
      square.style.backgroundColor = "";
   });
   upNextTetrominoes[nextRandom].forEach((index) => {
      displaySquares[displayIndex + index].classList.add("tetromino");
      displaySquares[displayIndex + index].style.backgroundColor =
         colors[nextRandom];
   });
}

//Game Over functions
function tryGameOver() {
   (checkGameOver()) && doItAtGameOver();
}

function checkGameOver(){
   const {squares} = getDOMElements();
   return (squares[34].classList.contains("taken") ||
           squares[35].classList.contains("taken") ||
           squares[36].classList.contains("taken"));
}

function doItAtGameOver(){
   actionsAtGameOver();
   renderAtGameOver();
}

function actionsAtGameOver(){
      stopTimers();
      deactivateGameButtons();
}

function stopTimers(){
      gameTimer.stop();
      speeder.stop();
}

function renderAtGameOver(){
      overScore.innerHTML = "SCORE : " + score;
      gameOverDiv.style.display = "block";
      console.log("GAME OVERED");
}

///

//TEMPLATES
function TemplateForGrid() {
   let gridDivs = "";
   for (let count = 0; gridNumbers > count; count++) {
      let classnames = gridClassCalculator(gridNumbers, count);
      gridDivs += TemplateForGridDiv(
         classnames,
         count,
         Math.ceil((count + 1) / 10)
      );
   }
   return `
         ${gridDivs}   
   `;
}

function gridClassCalculator(gridNumbers, count) {
   let classArray = [];
   count <= 29 && classArray.push("hidden");
   gridNumbers - width <= count && classArray.push("taken");
   gridNumbers - width > count &&
      width * 3 <= count &&
      classArray.push("activegameplace");
   divisible(count, width) === count && classArray.push("first-in-row");
   numberLastCharacter(count) === 9 && classArray.push("last-in-row");

   let classnames = classArray.join(" ");
   return classnames;
}

function TemplateForGridDiv(classnames, count, oneline) {
   return `
      <div class="${classnames}" data-serialnumber="${count}" data-oneline="${oneline}"></div>
   `;
}
////

////HELPERS
function divisible(dividend, divisor) {
   if (dividend % divisor == 0) {
      return dividend;
   } else {
      var num = dividend + (divisor - (dividend % divisor));
      return num;
   }
}

function numberLastCharacter(number) {
   let string = number.toString();
   return parseInt(string[string.length - 1]);
}

function recurs(num, max, recurses = []){
     if(max >= num){
        return recurs(num+1, max, [...recurses, num])
    }
    return recurses 
}

function IntervalEncapsulator(fn, time) {
   let timer = false;
   this.start = function () {
      timer = !this.status() ? setInterval(fn, time) : timer;
   };
   this.stop = function () {
      clearInterval(timer);
      timer = false;
   };
   this.setTime = function(newTime){
		if(!isNaN(newTime) && newTime != undefined && newTime ){
			console.log('SET NEW TIME ::: ' + newTime)
			const oldStatus = this.status()
			this.stop();
			time = newTime;
			oldStatus && this.start()
		}
	}
   this.status = function () {
      return timer !== false;
   };
   this.checkInterval = function(){ return time};
   this.showDefault = time;
}

function backcounter(count){
	return {
		doBackCount: doBackCount(),
		getActualCount: getActualCount(),
		getDefaultCount: getDefaultCount(),
		restartBackCount: restartBackCount(count)
	}
	function doBackCount(){
		return function() {
			return count = (count === 0)? 0 : count - 1;	
		}
	}
	function getActualCount(){
		return function() {
			return count;	
		}
	}
	function getDefaultCount(){
		return count;	
	}
	function restartBackCount(recount){
		return function() {
			return count = recount;
		}
	}
}

//COMMANDS for Backcounter
//run.doBackCount()
//run.getActualCount()
//run.getDefaultCount
//run.restartBackCount()
//

function getDOMElements(){
   return {
      squares: Array.from(document.querySelectorAll(".grid div")),
      firstSquare: function(){ return this.squares[30] },
      squaresInActiveGameplace: Array.from(grid.querySelectorAll(".activegameplace")),
      firstSquareInRow: document.querySelectorAll(".first-in-row"),
      lastSquareInRow: document.querySelectorAll(".last-in-row"),
      music: document.getElementById("myAudio")
   }
}

//EVENTS

function fasterDown() {
   speeder.start();
}

function fasterDownStop() {
   speeder.stop();
}

function extraFastDown(){
   if(!lockCheck()){tryFreeze(); setTimeout(function(){ extraFastDown(); }, 25);}
}

function control(e) {
   e.keyCode === 37 && moveLeft();
   e.keyCode === 38 && rotate();
   e.keyCode === 39 && moveRight();
   e.keyCode === 35 && extraFastDown();
}

function controlDown(e) {
   e.keyCode === 40 && fasterDown();
   document.removeEventListener("keydown", controlDown);
}

function controlUp(e) {
   e.keyCode === 40 && fasterDownStop();
   document.addEventListener("keydown", controlDown);
}

function activateGameButtons() {
   document.addEventListener("keydown", control);
   document.addEventListener("keydown", controlDown);
   document.addEventListener("keyup", controlUp);
   leftMove.addEventListener("mousedown", moveLeft);
   rightMove.addEventListener("mousedown", moveRight);
   downMove.addEventListener("click", tryFreeze);
   downMove.addEventListener("touchstart", fasterDown);
   downMove.addEventListener("touchend", fasterDownStop);
   downMove.addEventListener("mousedown", fasterDown);
   downMove.addEventListener("mouseup", fasterDownStop);
   
   rotation.addEventListener("mousedown", rotate);
   resetButton.addEventListener("click", initGame);
   console.log("GAME BUTTONS ACTIVATED");
}

function deactivateGameButtons() {
   if (controllButtonsDeactivable) {
      document.removeEventListener("keydown", control);
      document.removeEventListener("keydown", controlDown);
      document.removeEventListener("keyup", controlUp);
      controllButtonsSection.innerHTML = controllButtonsSection.innerHTML;
      console.log("GAME BUTTONS DEACTIVATED");
   }
}

//Login Button Event
loginBtn.addEventListener("click", () => {
   userzone.classList.remove("hidden");
});

//UserZone Close Button Event
closeUserZone.addEventListener("click", () => {
   userzone.classList.add("hidden");
});

//START/PAUSE button EVENT
startBtn.addEventListener("click", () => {
   let gameTimerStatus = gameTimer.status();
   !gameTimerStatus && activateGameButtons();
   gameTimerStatus && deactivateGameButtons();
   displayShape();
   startpauseSwitcher(gameTimer.status());
   startpauseButtonSwitcher(gameTimer.status());
});
   
function startpauseSwitcher(gameTimerStatus) {
   console.log(" SWITCH START / PAUSE ");
   (!gameTimerStatus) && gameTimer.start();
   (gameTimerStatus) && gameTimer.stop();
}

function startpauseButtonSwitcher(gameTimerStatus) {
   startBtn.innerHTML = (!gameTimerStatus)? "▶ RE-START" : "⏸ PAUSE";
}
   
 /////

//Mute button event
muteButton.addEventListener("click", () => {
   muteSwitcher(musicInterval.status());
   muteButtonSwitcher(musicInterval.status());
});

function muteSwitcher(musicIntervalStatus) {
   console.log("MUTE ON / OFF");
   musicIntervalStatus === false && musicInterval.start();
   musicIntervalStatus === true && musicInterval.stop();
   getDOMElements().music.pause();
}

function muteButtonSwitcher(musicIntervalStatus) {
   muteButton.innerHTML = musicIntervalStatus === false ? "🔇 MUTE" : "🔈 MUTE";
}

function playMusic() {
   getDOMElements().music.play();
}

///

///Options Button events
document.addEventListener("keydown", panelcontrol);
optionsButton.addEventListener('click', toggleOptionPanel);
closeOPtions.addEventListener('click', toggleOptionPanel);

function panelcontrol(e){
   e.keyCode === 79 && toggleOptionPanel();
}

function toggleOptionPanel(){
   optionspanel.classList.toggle('hidden');
   paneloverlay.classList.toggle('blured');
}
///

///Panel Overlay Events
function closeAllModalsAndOverlay(){
   console.log("CLOSE ALL MODALS")
   paneloverlay.classList.remove('blured');
   [...document.querySelectorAll('.modal')].map((modals) => modals.classList.add('hidden'));
}
///

//Invert chkbox event
invertCheckBox.addEventListener("change", transformTheGrid);

function transformTheGrid() {
   grid.style.transform = (checkGridInTransform()) ? "" : "rotate(180deg)";
}

function checkGridInTransform(){
   return grid.style.transform === ""? false : true;
}
///

//Reward chkbox event
rewardCheckBox.addEventListener("change", () => { rewardFunctionSwitch(checkRewardExtra()) });

function rewardFunctionSwitch(checkRewardExtra) {
   (checkRewardExtra) && doItIfRewardExtra();
   (!checkRewardExtra) && doItIfNotRewardExtra();
   console.log(`ACTUAL REWARD: ${localStorage.getItem("storedReward")}`);
}

function doItIfRewardExtra(){
   localStorage.setItem("storedReward", false);
   rewardPicDivSetHeight(0);
}

function doItIfNotRewardExtra(){
   localStorage.setItem("storedReward", true);
   setRewardPic();
}
////

//WeirdMode checkbox event
weird.addEventListener("change", ()=> {
      localStorage.setItem("storedWeirdMode", weird.checked);
   }
);
///

//DevFunctions
function numberisedSquares(){
   let counter = 0;
   getDOMElements().squares.map((square) => (square.innerHTML = counter++));
}

function numberisedOneline(){
   getDOMElements().squares.map((square) => (square.innerHTML = square.dataset.oneline));
}

function squareToOneline(square){ const {squares} = getDOMElements(); return squares[square].dataset.oneline; }

function squareToOnelineNum(square){ return parseInt(squareToOneline(square)); }

function addOnelineToTaken(oneline){ [...grid.querySelectorAll(`[data-oneline="${oneline}"]`)].map((cell) => cell.classList.add('taken')); }

function addToTakenToSquares(squaresStart, squaresMax){
   return recurs(squaresStart, squaresMax).map((item)=> squares[item].className += " taken tetromino")
}

function squaresInOneline(oneline){ return getOneline(oneline).map(cell => parseInt(cell.dataset.serialnumber)); }

function makeTestLines(){
   addToTakenToSquares(200, 208)
   addToTakenToSquares(210, 218)
   addToTakenToSquares(220, 228)
   addToTakenToSquares(230, 238)
}

function arrToArg(squaresArray){
   return squaresArray.map((squareArray) => squareArray)
}

function addOnelineToTakenAndColorizeThat(oneline){ addOnelineToTaken(oneline); turnToPinkAllTakensOnActiveGameplace(); }

function removeAllTakenFromActiveGameplace(){ 
   const {squaresInActiveGameplace} = getDOMElements();
   squaresInActiveGameplace.map((square) => { 
   square.classList.remove('taken'); turnToPinkAllTakensOnActiveGameplace(); 
   })
}

function removeOneline(oneline){ getOneline(oneline).map((cell) => cell.remove()) }

function getAllActiveGameplaces(){
   return [...grid.querySelectorAll('.activegameplace')]
}

function turnToPinkActiveGameplace(){ getAllActiveGameplaces().map((cell) => cell.style.backgroundColor = "pink"); }

function turnToPinkAllTakensOnActiveGameplace(){ getAllActiveGameplaces().map((cell) => cell.style.backgroundColor = cell.classList.contains('taken')? "pink" : "" ); }

function removeOnelineFromTakenAndUnColorizeThat(oneline){ removeOnelineFromTaken(oneline); turnToPinkAllTakensOnActiveGameplace(); }

function getOnelineNodes(oneline){
   return grid.querySelectorAll(`.activegameplace[data-oneline="${oneline}"]`)
}

const getCurrentInSquare = () => [(parseInt(currentPosition) + parseInt(current[0])), (parseInt(currentPosition) + parseInt(current[1])), (parseInt(currentPosition) + parseInt(current[2])), (parseInt(currentPosition) + parseInt(current[3]))]

const getNextRotatedCurrentInSquare = () => [(parseInt(currentPosition) + parseInt(nextRotatedCurrent[0])), (parseInt(currentPosition) + parseInt(nextRotatedCurrent[1])), (parseInt(currentPosition) + parseInt(nextRotatedCurrent[2])), (parseInt(currentPosition) + parseInt(nextRotatedCurrent[3]))]

function checkCurrent() {
   const {squares} = getDOMElements();
   console.log(
      "CURRENT TETRROMINO:  " +
         tetrominoNames[random] +
         " - ACTUAL RANDOM: " +
         random +
         " - PROFILE: " +
         current +
         "CURRENT POSITION: " +
         currentPosition
   );

   // Show actual current divlist on Console
   current.forEach((index) => {
      console.log(squares[currentPosition + index]);
   });

   console.log("SQUARES NUMBERS : " + getCurrentInSquare().join(' - ') + " ! ");

   nextRotation = currentRotation === 3 ? 0 : currentRotation + 1;
   nextRotatedCurrent = theTetrominoes[random][nextRotation];

   console.log("Next Rotation: " + nextRotation);

   console.log("NEXT ROTATED SQUARES NUMBERS: " + getNextRotatedCurrentInSquare().join(' - ') + " ! ");

   // Show next rotation divlist on Console
   nextRotatedCurrent.forEach((index) => {
      console.log(squares[currentPosition + index]);
   });
}

} //MAIN END
})(); //IIFE END
